package com.chimpim.myocrdemo

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.TextRecognizer
import com.google.mlkit.vision.text.chinese.ChineseTextRecognizerOptions
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.permissionx.guolindev.PermissionX
import com.wld.mycamerax.util.CameraConstant
import com.wld.mycamerax.util.CameraParam

class MainActivity : AppCompatActivity() {

    private lateinit var tvText: TextView
    private lateinit var btnGoCamera: Button
    private lateinit var imageView: ImageView

    private lateinit var recognizer: TextRecognizer

    private val mCameraParamBuilder = CameraParam.Builder()
        .setShowFocusTips(false)
        .setActivity(this@MainActivity)
        .setFocusViewColor(Color.TRANSPARENT)
        .setCameraTipText("请将内容置于取景框")

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initRecognizer()
    }

    private fun initRecognizer(){
        recognizer = TextRecognition.getClient(ChineseTextRecognizerOptions.Builder().build())
        //识别资源文件中的例图
        val task = recognizer.process(BitmapFactory.decodeResource(resources, R.drawable.eg), 0)
        task.addOnSuccessListener {
            tvText.text = it.text
        }.addOnFailureListener {
            tvText.text = it.message
        }
    }

    private fun initView(){
        tvText = findViewById(R.id.tv_text)
        btnGoCamera = findViewById(R.id.btn_go_camera)
        imageView = findViewById(R.id.image_view)
        btnGoCamera.setOnClickListener {
            requestPermissionToCamera()
        }
    }

    private fun requestPermissionToCamera(){
        PermissionX.init(this@MainActivity).permissions(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
        ).request { allGranted, _, deniedList ->
            if (allGranted) {
                mCameraParamBuilder.build()
            } else {
                Toast.makeText(
                    applicationContext,
                    "These permissions are denied: $deniedList",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //requestCode默认是CameraConstant.REQUEST_CODE ，当然也可以在上面的CameraParam创建的时候
        //调用setRequestCode修改
        if (requestCode == CameraConstant.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            //获取图片路径
            val picturePath = data!!.getStringExtra(CameraConstant.PICTURE_PATH_KEY)
            val bitmap = BitmapFactory.decodeFile(picturePath)
            //显示出来
            imageView.setImageBitmap(bitmap)
            //识别文字
            val t = recognizer.process(bitmap, 0)
            t.addOnSuccessListener {
                tvText.text = it.text
            }.addOnFailureListener {
                tvText.text = it.message
            }
        }

    }
}